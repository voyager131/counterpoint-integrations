# Counterpoint Integrations

This NodeJS project provides the glue that bridges NCR CounterPoint with a number of web stores (Woocommerce for now, Shopify and others to come later, etc..) via the REST APIs associated with NCR CounterPoint and each of the web stores.  Most notable is the fact that the NCR CounterPoint API is open source and freely available, but with scarse documentation and almost no examples of how it works.

The 40,000 ft overview of this project is to take the orders from a Wordpress site that uses WooCommerce for its storefront and push them into the NCRCounterPoint system where they can be accounted for and sent to the warehouse for fullfilment.

See the wiki for further documentation.

