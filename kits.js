exports.kits1 =
{
  'SPGW500-K-801': {
      ITEM_NO: 'SPGW-T-801',
      QTY_SOLD: 5
    } ,
  'FEGW500-K-801': {
      ITEM_NO: 'FEGW-T-801',
      QTY_SOLD: 5
    } ,
  'DRGW500-K-801': {
      ITEM_NO: 'DRGW-T-801',
      QTY_SOLD: 5
    } ,
  'MFGW500-K-801': {
      ITEM_NO: 'MFGLOW-O-801',
      QTY_SOLD: 5
    } ,
  'PHGW500-K-801': {
      ITEM_NO: 'PHGW-T-801',
      QTY_SOLD: 5
    } ,
  'AIR10-K-801': {
      ITEM_NO: 'AIRDM-O-801',
      QTY_SOLD: 10
    }
}
exports.kits2 =
{
  'SPWT-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'SPTB-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'SPTR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'SPGR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'DRTW-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DRTB-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DRTR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DRGR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'FEWK-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'FETB-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'FETR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'FERC-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'MFWT-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'MFTR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'MFTB-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'MFGR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'BMH-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DBM-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'DMB20-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DMBOY-O-801',
      PRC: 0,
      QTY_SOLD: 20,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'MFHK2-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'MFHK1-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'BMH-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'BMP-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DBM-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'DMN20-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DMNS-O-801',
      PRC: 0,
      QTY_SOLD: 20,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'DMSW9-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DMS9W-P-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DMS9-C-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'DMSW4-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DMS4W-P-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'DMS4-C-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ],
'PHWT-K-801': [
    {
      LIN_TYP: 'O',
      ITEM_NO: 'PHTB-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'PHTR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    },
    {
      LIN_TYP: 'O',
      ITEM_NO: 'PHGR-O-801',
      PRC: 0,
      QTY_SOLD: 1,
      USR_ENTD_PRC: 'Y',
      HAS_PRC_OVRD: 'Y'
    }
  ]
}
