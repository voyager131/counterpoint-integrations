const https = require('https');
exports.config = {
    baseURL: 'https://hyour-cpointapi.domain.com:52000',
    timeout: 10000,
    headers: { APIKey: 'your_APIKey',
	    rejectUnauthorized: false
    },
    auth: {
        username: 'CP_username',
	password: 'mysecretpw'
    },
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
	})
    };
