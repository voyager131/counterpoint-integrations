const https = require('https');
exports.config = {
    baseURL: 'https://username:passwd@mydomain.myshopify.com/admin/api/2021-07',
    timeout: 10000,
    headers: { APIKey: 'myAPIKey',
	    rejectUnauthorized: false
    },
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
	})
    };
