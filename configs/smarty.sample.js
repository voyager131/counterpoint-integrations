
const SmartyStreetsSDK = require("smartystreets-javascript-sdk");
const SmartyStreetsCore = SmartyStreetsSDK.core;
exports.Lookup = SmartyStreetsSDK.usStreet.Lookup;

let authId = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
let authToken = "xxxxxxxxxxxxxxxxxxxx";

let clientBuilder = new SmartyStreetsCore.ClientBuilder(new SmartyStreetsCore.StaticCredentials(authId, authToken));
exports.client = clientBuilder.buildUsStreetApiClient();

