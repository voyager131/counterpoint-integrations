exports.config = {
    user: 'dbuser',
    password: 'dbuserpw!',
    server: 'your-cpointdb.domain.com',
    database: 'yoursecretdbpw',
    port: 1433,
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 1000
      },
    options: {
      encrypt: true,
      trustServerCertificate: true
      }
}; 
