#!/bin/bash
. $HOME/.nvm/nvm.sh
set -x
if [ -f /home/dbidwell/skipimport ] ; then
	exit
fi
touch /tmp/cpi.log
./bin/prerun.sh
./bin/runwcmain.pl
