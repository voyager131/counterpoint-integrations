const getwoo = require('./modules/wcra.js');		// vop DC WooCommerce
const myaxios = require('./modules/axios.js');		// CounterPoint API
const sqlobj = require('./modules/asyncminimal.js');	// mssql
const mysql = require('./modules/mysql2.js');
const printers = require('./printers.js');
const bundledrop = require('./bundledrop.js');
const cpitems = require('./cpitems.js');
const wcutils = require('./wcutils.js');
const datab = require('./bundles.js');
const kits = require('./kits.js');
const chktkt = require('./chktkt.js');
const sales = require('./sales.js');

let shipping_method_id = "";
let verbose = 0;
let debug = 0;
let page = 1;
let bundles = 1;
let add2id = 0;
let dopostorder = 1;
let evarray=[];
//let evs = ['SP','DR','FE','VBS','RS','SE','PH','TA','DM'];
//let defev="SP";
let defeventprojcode;
//const eventprojcodes = {
//    "RS":	"8048365",
//    "DR":	"8048420",
//    "SE":	"8048610",
//    "PH":	"8048615",
//    "TA":	"8048625",
//    "SP":	"8048627",
//    "FE":	"8048636",
//    "DM":	"8050310",
//    "VBS":	"8050310"
//    }
const provinces = {
    "AB" : "1",
    "BC" : "1",
    "MB" : "1",
    "NB" : "1",
    "NL" : "1",
    "NS" : "1",
    "NU" : "1",
    "ON" : "1",
    "PE" : "1",
    "QC" : "1",
    "SK" : "1",
    "YK" : "1"
    }
const statet = {
    "AL" : "1",
    "AK" : "1",
    "AZ" : "1",
    "AR" : "1",
    "AS" : "1",
    "CA" : "1",
    "CO" : "1",
    "CT" : "1",
    "DE" : "1",
    "DC" : "1",
    "FL" : "1",
    "GA" : "1",
    "GU" : "1",
    "HI" : "1",
    "ID" : "1",
    "IL" : "1",
    "IN" : "1",
    "IA" : "1",
    "KS" : "1",
    "KY" : "1",
    "LA" : "1",
    "ME" : "1",
    "MD" : "1",
    "MA" : "1",
    "MI" : "1",
    "MN" : "1",
    "MS" : "1",
    "MO" : "1",
    "MT" : "1",
    "NE" : "1",
    "NV" : "1",
    "NH" : "1",
    "NJ" : "1",
    "NM" : "1",
    "NY" : "1",
    "NC" : "1",
    "ND" : "1",
    "MP" : "1",
    "OH" : "1",
    "OK" : "1",
    "OR" : "1",
    "PA" : "1",
    "PR" : "1",
    "RI" : "1",
    "SC" : "1",
    "SD" : "1",
    "TN" : "1",
    "TX" : "1",
    "TT" : "1",
    "UT" : "1",
    "VT" : "1",
    "VA" : "1",
    "VI" : "1",
    "WA" : "1",
    "WV" : "1",
    "WI" : "1",
    "WY" : "1"
    }
let bundleid = [];

let work=0;	// have we processed any orders?
let manual=0;
for (const a of process.argv.slice(2)){
    if(a === "verbose"){
        verbose = 1;
	dopostorder = 0;
	}
    else if(a === "debug"){
        debug = 1;
        verbose = 1;
	dopostorder = 0;
	}
    else if(a === "dopostorder" || a === "postorder"){
        dopostorder = 1;
	}
    else if(a === "page"){
        page += 1;
	}
    else if(a === "bundles"){
        bundles = 0;
	}
    else if(a === "add2id"){
        add2id += 1;
	}
//    else if(a === "smarttest"){
//        smarttest += 1;
//	dopostorder=0;
//	}
    else manual = parseInt(a);
    }
console.log(`verbose=${verbose}, dopostorder=${dopostorder} debug=${debug},manual=${manual}`);
if(chktkt.chktkt === false){
    console.log("stop");
    return;
    }
getwoo.getwoo(`orders?page=${page}`,100,(orders) => {
    let lpc=0;
    let stop=0;
    for (const s of orders.data){
        lpc++;
	if(stop === 1) break;
	if(debug === 1 && manual !== 0){
	    console.log(`lpc = ${lpc}, id=${s.id}, status = ${s.status}`);
	    if(s.id !== manual) continue;
	    stop=1;
            }
      else {
	  if(s.status !== "ready") continue;
	  console.log(`${lpc}: order #${s.id}, status=${s.status}`);
	  stop=1;
	  work=1;
          }
      s.cunumber="CU801W-"
      if(s.customer_id < 1000000) s.cunumber += '0';
      if(s.customer_id < 100000) s.cunumber += '0';
      if(s.customer_id < 10000) s.cunumber += '0';
      if(s.customer_id < 1000) s.cunumber += '0';
      if(s.customer_id < 100) s.cunumber += '0';
      if(s.customer_id < 10) s.cunumber += '0';
      s.cunumber += s.customer_id;
      s.tktno="W801-";
      if(s.id < 10000) s.tktno += '0';
      if(s.id < 1000) s.tktno += '0';
      if(s.id < 100) s.tktno += '0';
      if(s.id < 10) s.tktno += '0';
      s.tktno+=s.id+add2id;
      console.log(`processing wc order #${s.id} for ${s.cunumber} with ${s.tktno}`);
      if(verbose === 1) console.log(s);
      if(verbose === 1){
          console.log("shipping_lines");
	  console.log(s.shipping_lines);
	  console.log(s.shipping_lines[0].meta_data);
	  }
      let skip=0;
      for (const i of s.line_items){
              if(verbose === 1){
                  console.log("line_item");
                  console.log(i);
                  }
	  if(i.sku.includes("801-1"))i.sku=i.sku.replace('801-1','801');
	  if(i.sku === '' || cpitems.cpitems[i.sku] !== '1'){
	      skip=1;
	      console.log(`skipping, sku=${i.sku}, product id=${i.product_id} for order #${s.id} not found in CounterPoint inventory`);
	      getwoo.postnoteandstatus(s,`sku=${i.sku} with product id=${i.product_id} not found in CounterPoint inventory`,'cp-stalled','1');
	      }
	  }
      if(skip === 1) continue;
//      defeventprojcode=eventprojcodes[defev];
      defeventprojcode="8051400";
      getwoo.getwoo(`customers/${s.customer_id}`,1, (r) => {
	  if(verbose === 1)console.log(`WC id = ${s.customer_id}`);
	  let data = r.data;
          let meta = data.meta_data;
	  if(verbose === 1)console.log(meta);
	  let church_pastor=wcutils.getkv(meta,"church_pastor");
	  church_pastor=church_pastor.trim();
	  let namearray = church_pastor.split(" ");
	  let church = {
	      "company":	wcutils.getkv(meta,"company"),
	      "contct_1":	church_pastor,
	      "first_name":	namearray[0],
	      "last_name":	namearray[namearray.length-1],
	      "address_1":	wcutils.getkv(meta,"church_address_1"),
	      "address_2":	wcutils.getkv(meta,"church_address_2"),
	      "city":		wcutils.getkv(meta,"church_city"),
	      "state":		wcutils.getkv(meta,"church_state"),
	      "country":	wcutils.getkv(meta,"church_country"),
	      "postcode":	wcutils.getkv(meta,"church_postcode"),
	      "phone":		wcutils.getkv(meta,"church_phone")
	      };
	  if(church.first_name === '')church.first_name=s.billing.first_name;
	  if(church.last_name === '')church.last_name=s.billing.last_name;
	  church.company = wcutils.shortencompany(church.company);
	  console.log(`church.company=${church.company}, length=${church.company.length}`);
	  church.first_name = wcutils.shortencompany(church.first_name);
          if(church.address_1.length === 0 || church.city.length === 0 ||
            church.state.length === 0 || church.country.lengh === 0 ||
            church.city.length > 20){
              church=s.shipping;
	      if(church.first_name === '' && church.address_1 === ''){
	          church=s.billing;
		  }
	      }
          let cpu=addresschecks(s,church);
          if(cpu === 0) return; // there was an error, skip it
          cpu=wcu2cpu(s,church);
          if(cpu === 0) return; // there was an error, skip it
          if(verbose === 1){
              console.log("PS_DOCS");
              console.log(cpu.PS_DOCS);
              console.log("line items");
              console.log(cpu.PS_DOCS.PS_DOC_HDR.PS_DOC_LIN);
	      console.log("AR_CUST");
	      console.log(cpu.AR_CUST);
	      }
          if(dopostorder === 0)return;
          myaxios.myget("/Customer",s.cunumber, (response) => {
	      if(verbose === 1)console.log(`GET /Customer/${s.cunumber} returned a status of ${response.status}`);
              if( response.status !== '' ){
                  if(verbose === 1){
//		      console.log(response);
		      console.log(`${s.cunumber} not found, you need to make it`);
		      }
	          const arg = { "AR_CUST": cpu.AR_CUST};
	          myaxios.mypost("/Customer",arg, (response) => {
	              if(verbose === 1)console.log(`POST /Customer ${s.cunumber}`);
		      if(response.status === ""){
		          if(verbose === 1)console.log("create succeeded");
		          }
		      else {
		          console.log(response);
	                  getwoo.postnoteandstatus(s,`create CP customer failed for ${s.cunumber} on order #${s.id} with message of ${response}`,'cp-stalled','1');
		          return(0);
		          }
		      postorder(cpu.PS_DOCS,s);
		      });
              } else {
                  postorder(cpu.PS_DOCS,s);
	      }
          });
        });
    }
    if(work === 1) console.log("continue");
    else console.log("stop");
});
if(work === 1) console.log("continue");

function postorder(p,s){
    let orderno="";
    let ticketno="";
//    if(verbose === 1){
        console.log("in postorder(p)");
        console.log("PS_DOCS");
        console.log(p);
        console.log("line items");
        console.log(p.PS_DOC_HDR.PS_DOC_LIN);
        console.log("PS_DOC_HDR_MISC_CHRG items");
        console.log(p.PS_DOC_HDR.PS_DOC_HDR_MISC_CHRG);
        console.log("PS_DOC_PMT items");
        console.log(p.PS_DOC_HDR.PS_DOC_PMT);
        console.log("PS_DOC_TAX items");
        console.log(p.PS_DOC_HDR.PS_DOC_TAX);
//	}
    if(dopostorder === 0)return;
    if(verbose === 1)console.log("Calling myaxios.mypost(/Document....)");
    let ordermesg = '';
    let ticketmsg = '';
    let m = [];
    checknexttktnos( (stat) => {	// see if we have in internally consistant database
        if(stat === "bad")return(0);	// CounterPoint database is bad
	// Discuss propriety of fixing CP database
    myaxios.mypost("/Document",p, (response) => {
        if(verbose === 1)console.log("POST /Document");
	let docid="";
	if(response.status === ""){
	if(verbose === 1){
	    console.log(response.data);
            console.log("create succeeded");
	    }
	let d=response.data.Documents[0];
	    docid = d.DOC_ID;
	    console.log(`reading ${d.DOC_ID} for ${d.reference}`);
//	    `SELECT TKT_NO from dbo.PS_DOC_HDR where DOC_ID = '${d.DOC_ID}'`,
	    const sqlobjt0= sqlobj.querysql(
	    `SELECT * from dbo.PS_DOC_HDR where DOC_ID = '${d.DOC_ID}'`,
	    (dr) => {
	       m[d.DOC_ID]=dr.recordset[0].TKT_NO;
	       console.log(dr.recordset[0]);
	       if(d.reference === 'Order'){
	           orderno = dr.recordset[0].TKT_NO;
		   ordermesg = `order # ${orderno}`;
		   if(verbose === 1)console.log(`TKT_NO = ${orderno}`);
	               }
		   else {
		       ticketno = dr.recordset[0].TKT_NO;
		       if(verbose === 1)console.log(`TKT_NO = ${ticketno}`);
		       if(ordermesg === ""){
		           ticketmsg = `ticket # ${ticketno}`;
		           } else {
		           ticketmsg = `and ticket # ${ticketno}`;
		           }
		         }
	            d=response.data.Documents[1];
	            docid = d.DOC_ID;
		    console.log(`reading ${d.DOC_ID} for ${d.reference}`);
//		`SELECT TKT_NO from dbo.PS_DOC_HDR where DOC_ID = '${d.DOC_ID}'`,
		    const sqlobjt1= sqlobj.querysql(
		`SELECT * from dbo.PS_DOC_HDR where DOC_ID = '${d.DOC_ID}'`,
		    (dr) => {
	               console.log(dr.recordset[0]);
		       m[d.DOC_ID]=dr.recordset[0].TKT_NO;
	               if(d.reference === 'Order'){
		          orderno = dr.recordset[0].TKT_NO;
		          ordermesg = `order # ${orderno}`;
		          if(verbose === 1)console.log(`TKT_NO = ${orderno}`);
	                  }
		       else {
		           ticketno = dr.recordset[0].TKT_NO;
		           if(verbose === 1)console.log(`TKT_NO = ${ticketno}`);
		           if(ordermesg === ""){
		               ticketmsg = `ticket # ${ticketno}`;
		               } else {
		               ticketmsg = `and ticket # ${ticketno}`;
		               }
			    }
	    console.log(`calling checktknos with ${orderno} and ${ticketno}`);
	    checktktnos(orderno,ticketno,(o,t) => {
	        });
	    if(orderno !== '')ordermesg = `order # ${orderno}`;
	    if(ordermesg === ""){
		ticketmsg = `ticket # ${ticketno}`;
		} else {
		ticketmsg = `and ticket # ${ticketno}`;
		}
	    for(const d of response.data.Documents){
		if(verbose === 1){
		    console.log("response");
		    console.log(d);
		    }
                const sql1q = `INSERT INTO PS_DOC_HDR_EXT (DOC_ID_EXT,USR_PROJ_COD) VALUES ('${d.DOC_ID}','${defeventprojcode}')`;
		sqlobj.querysql(sql1q, (r) => {
		    });
		const sql2q = `UPDATE PS_DOC_LIN_EXT set USR_PROJ_COD = '${defeventprojcode}' where DOC_ID_EXT = '${d.DOC_ID}'`;
		sqlobj.querysql(sql2q, (r) => {
		    });
		if(verbose === 1)console.log(`reference = ${d.reference}, DOC_ID = ${d.DOC_ID}`);
		docid = d.DOC_ID;
//	        if(d.reference === 'Order'){
//		  const sql3q = `UPDATE PS_DOC_HDR_TOT SET HAS_TAX_OVRD = 'A', TAX_AMT = '${s.total_tax}' where DOC_ID = '${d.DOC_ID}' AND TOT_TYP = 'O'`;
//		  sqlobj.querysql(sql3q, (r) => {
//		      });
//		   }
	            if(docid === '') continue;
		 const myq = `INSERT INTO doc_id2sale_id (doc_id,orderno,storeid,sale_id,shipping_method) VALUES (${docid},'${m[docid]}','vopdcwc',${s.id},'${shipping_method_id}')`;
		mysql.doquery(myq, (r) => {
		});
		const sql4q = `UPDATE dbo.AR_CUST SET PROF_COD_2 = '02' WHERE CUST_NO = '${s.cunumber}'`;
		sqlobj.querysql(sql4q, (r) => {
		      });
	    }
	const sqlpdpp = `INSERT INTO PS_DOC_PMT_PROMPT (DOC_ID, PMT_SEQ_NO, PROMPT_SEQ_NO,PROMPT_TXT,PROMPT_VAL) values ( '${docid}','1','1','${s.id}','Order Number')`;
	sqlobj.querysql(sqlpdpp, (r) => {
	     if(r.status !== 200){
		}
	    });
	if(verbose === 1) console.log(`ticketno ${ticketno} orderno ${orderno}`);
        getwoo.postnoteandstatus(s,`updating status to infulfillment with ${ordermesg} ${ticketmsg}`,'infulfillment','0');
    	if(verbose === 1)console.log(`updating status to infulfillment with ${ordermesg} ${ticketmsg}`);
        });
        });
        } else {
	    getwoo.postnoteandstatus(s,`Post of order failed for ${s.cunumber} on order #${s.id} with message of ${response.response.data.Message}`,'cp-stalled','1');
	    return;
	}
      });
      });
    }
function addresschecks(s,church){
    let sshipto=s.shipping;
    let billto=s.billing;
    s.shipping.state=s.shipping.state.toUpperCase();
    s.billing.state=s.billing.state.toUpperCase();
    church.state=church.state.toUpperCase();
    if(billto.city === church.city)church.state=billto.state;
    if(sshipto.first_name === "" && sshipto.address_1 === ""){
        sshipto=s.billing;
	}
    billto.company=wcutils.shortencompany(billto.company);
    billto.first_name=wcutils.shortencompany(billto.first_name);
    sshipto.company=wcutils.shortencompany(sshipto.company);
    sshipto.first_name=wcutils.shortencompany(sshipto.first_name);
    if(sshipto.first_name.length >15 || billto.first_name.length >15){
	getwoo.postnoteandstatus(s,`first names can't be longer than 15 letters shipto: ${sshipto.first_name} or billto: ${billto.first_name}`,'cp-stalled','1');
	return(0);
        }
    if(billto.last_name.length > 25)billto.last_name=shortencompany(billto.last_name);
    if(sshipto.last_name.length > 25)sshipto.last_name=shortencompany(sshipto.last_name);
    if(church.address_1.length === 0 || church.city.length === 0 ||
    church.state.length === 0 || church.country.lengh === 0){
    	getwoo.postnoteandstatus(s,`church profile address is incomplete`,'cp-stalled','1');
	return(0);
	}
    if(sshipto.last_name.length > 25 || billto.last_name.length > 25 ){
	getwoo.postnoteandstatus(s,`last names can't be longer than 25 letters`,'cp-stalled','1');
	return(0);
        }
    if(church.last_name.length > 25){
	getwoo.postnoteandstatus(s,`church profile last names can't be longer than 25 letters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.company.length > 40 || billto.company.length > 40 ){
	getwoo.postnoteandstatus(s,`church name can't be more than 40 characters`,'cp-stalled','1');
	return(0);
        }
    if(church.company.length > 40){
	getwoo.postnoteandstatus(s,`church profile name can't be more than 40 characters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.address_1.length >40 || sshipto.address_2.length > 40
      || billto.address_1.length >40 || billto.address_2.length > 40){
	getwoo.postnoteandstatus(s,`address lines can't be more than 40 characters`,'cp-stalled','1');
	return(0);
        }
    if( church.address_1.length >40 || church.address_2.length > 40){
getwoo.postnoteandstatus(s,`church profile address lines can't be more than 40 characters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.city.length > 20){
	getwoo.postnoteandstatus(s,`shipping city names can't be more than 20 characters`,'cp-stalled','1');
	return(0);
        }
    if(billto.city.length > 20){
	getwoo.postnoteandstatus(s,`billing city names can't be more than 20 characters`,'cp-stalled','1');
	return(0);
        }
    if(church.city.length > 20){
	getwoo.postnoteandstatus(s,`church profile city names can't be more than 20 characters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.state.length > 10 || billto.state.length > 10 || church.state.length > 10){
	getwoo.postnoteandstatus(s,`State names can't be more than 10 characters, they should be 2 letters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.postcode.length > 15){
	getwoo.postnoteandstatus(s,`shipping ZIP codes can't be more than 15 characters`,'cp-stalled','1');
	return(0);
        }
    if(billto.postcode.length > 15){
	getwoo.postnoteandstatus(s,`billing ZIP codes can't be more than 15 characters`,'cp-stalled','1');
	return(0);
        }
    if(church.postcode.length > 15){
	getwoo.postnoteandstatus(s,`church profile ZIP codes can't be more than 15 characters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.phone.length > 25 || billto.phone.length > 25 ){
	getwoo.postnoteandstatus(s,`Phone numbers can't be more than 25 characters`,'cp-stalled','1');
	return(0);
        }
    if( church.phone.length > 25){
	getwoo.postnoteandstatus(s,`church profile Phone numbers can't be more than 25 characters`,'cp-stalled','1');
	return(0);
        }
    if(sshipto.country === '' || sshipto.country === 'US' || sshipto.country === 'United States' || sshipto.country === 'USA'){
    	sshipto.country = 'US';
	}
    if( billto.country === '' || billto.country === 'US' || billto.country === 'United States' || billto.country === 'USA'){
    	billto.country = 'US';
	}
    if(church.country === '' || church.country === 'US' || church.country === 'United States' || church.country === 'USA'){
    	church.country = 'US';
	}
    if(sshipto.country === 'Canada' || sshipto.country === 'CANADA'){
        shipto.country = 'CA';
	}
    if(billto.country === 'Canada' || billto.country === 'CANADA'){
        billto.country = 'CA';
	}
    if(church.country === 'Canada' || church.country === 'CANADA'){
        church.country = 'CA';
	}
    if(sshipto.country.length > 2 || billto.country.length > 2 || church.country.length > 2){
	getwoo.postnoteandstatus(s,`Country abbreviations can't be more than 2 characters`,'cp-stalled','1');
	return(0);
        }
    if((sshipto.country === 'US' && statet[sshipto.state] !== "1") ||
       (billto.country === 'US' && statet[billto.state] !== "1") ||
       (church.country === 'US' && statet[church.state] !== "1")){
	getwoo.postnoteandstatus(s,`State abbreviations should be 2 characters`,'cp-stalled','1');
	return(0);
        }
    if((sshipto.country === 'CA' && provinces[sshipto.state] !== "1") ||
       (billto.country === 'CA' && provinces[billto.state] !== "1") ||
       (church.country === 'CA' && provinces[church.state] !== "1")){
	getwoo.postnoteandstatus(s,`province abbreviations should be 2 characters`,'cp-stalled','1');
	return(0);
        }
    return(1);
    }
function wcu2cpu(s,church){
    let sshipto=s.shipping;
    let billto=s.billing;
    if(sshipto.first_name === "" && sshipto.address_1 === ""){
        sshipto=s.billing;
	}
    let nam = sshipto.first_name + " " + sshipto.last_name;
    let nams = nam;
    let namb = billto.first_name + " " + billto.last_name;
    let contctb=namb;
    let contcts=nam;
    let nams_typ = 'P';
    let namb_typ = 'P';
    if(billto.company !== ''){
    	namb = billto.company;
	namb_typ = 'B';
	}
    if(sshipto.company !== ''){
    	nams = sshipto.company;
	nams_typ = 'B';
	}
    let AR_SHIP_ADRS = {
            "CUST_NO":	s.cunumber,
	    "NAM":	nams,
	    "SHIP_NAM_TYP":	nams_typ,
	    "FST_NAM":	sshipto.first_name,
	    "LST_NAM":	sshipto.last_name,
	    "ADRS_1":	sshipto.address_1,
	    "ADRS_2":	sshipto.address_2,
	    "CITY":	sshipto.city,
	    "STATE":	sshipto.state,
	    "CNTRY":	sshipto.country,
	    "ZIP_COD":	sshipto.postcode,
	    "EMAIL_ADRS_1":	sshipto.email,
	    "PHONE_1":		sshipto.phone,
	    "STR_ID":		"801"
	    };
    let AR_CUST = {
	    "CUST_NO":		s.cunumber,
	    "NAM":		church.company,
	    "CUST_NAM_TYP":	'B',
	    "FST_NAM":		church.first_name,
	    "LST_NAM":		church.last_name,
	    "ADRS_1":		church.address_1,
	    "ADRS_2":		church.address_2,
	    "CITY":		church.city,
	    "STATE":		church.state,
	    "CNTRY":		church.country,
	    "ZIP_COD":		church.postcode,
	    "EMAIL_ADRS_1":	church.email,
	    "PHONE_1":		church.phone,
	    "STR_ID":		"801",
	    "SLS_REP":		"ECOM",
	    "CUST_TYP":		"C",
	    "PROF_COD_2":	"02"
	    };
	if(AR_SHIP_ADRS.EMAIL_ADRS_1 === undefined)AR_SHIP_ADRS.EMAIL_ADRS_1='';
	if(AR_CUST.EMAIL_ADRS_1 === undefined)AR_CUST.EMAIL_ADRS_1='';
	shipping_method_id = "";
	let method_id = "";
	let shippingtotal = s.shipping_total;
	if(s.shipping_lines.length > 0){
		if(verbose === 1)console.log(`method_id=${s.shipping_lines[0].method_id}, method_title=${s.shipping_lines[0].method_title}`);
		method_id=wcutils.shippingmap(s.shipping_lines[0].method_id,s.shipping_lines[0].method_title,sshipto.country);
		if(verbose === 1)console.log(`method_id=${method_id}`);
		shipping_method_id=method_id;
		}
	else {
	    if(sshipto.country === '' || sshipto.country === 'US'){
	        method_id='FEDEXGROUN';
		}
	    else method_id='PRIOR_INT';
	    }
	if(s.payment_method.includes('che')){
	    s.payment_method='check';
	    }
	if(s.payment_method === ''){
	    s.payment_method='check';
	    }
	if(sshipto.phone === '' && sshipto.address_1 === billto.address_1){
	    sshipto.phone = billto.phone;
	    }
	let taxbl= s.total-s.total_tax;
	if(s.shipping_tax === 0) {
	    taxable -= s.shipping_total;
	    }
	let refunds=0;
	for (const r of s.refunds){
		refunds += r.total;
		}
        for (const i of s.line_items){
	    const disc=i.subtotal - i.total;
	    if(disc !== 0){
	    	s.discount_total -= disc;
		}
	    }
	let discount = 0 - s.discount_total;
//	discount -= refunds;
	let PS_DOC_HDR = {
	    "STR_ID":		"801",
	    "STA_ID":		"801_TRAIN",
	    "CUST_NO":		s.cunumber,
	    "TKT_NO":		s.tktno,
	    "DRW_ID":		"ECOM",
	    "PFT_CTR":		"801-XXXXX",
	    "TKT_TYP":		"T",
	    "DOC_TYP":		"O",	// might become T
	    "USR_ID":		"ECOM",
	    "SLS_REP":		"ECOM",
	    "TAX_COD":		"AVALARA",
	    "NORM_TAX_COD":	"AVALARA",
	    "TAX_AMT":	s.total_tax,
	    "TAX_OVRD_REAS":	"AVALARA",
	    "TAX_EXEMPT_NO":	"NA",
	    "SHIP_VIA_COD":	method_id,
	    "CUST_PO_NO":	s.id,
	    "USR_ENTD_PRC":	"Y",
	    "HAS_PRC_OVRD":	"Y",
	    "BILL_TO_CONTACT": {
	        "NAM":			namb,
		"CONTCT_1":		contctb,
	        "FST_NAM":		billto.first_name,
	        "LST_NAM":		billto.last_name,
	        "ADRS_1":		billto.address_1,
	        "ADRS_2":		billto.address_2,
	        "CITY":			billto.city,
	        "STATE":		billto.state,
	        "CNTRY":		billto.country,
	        "ZIP_COD":		billto.postcode,
	        "PHONE_1":		billto.phone,
		"NAM_TYP":		namb_typ
		},
	    "SHIP_TO_CONTACT": {
	        "NAM":			nams,
		"CONTCT_1":		contcts,
	        "FST_NAM":		sshipto.first_name,
	        "LST_NAM":		sshipto.last_name,
	        "ADRS_1":		sshipto.address_1,
	        "ADRS_2":		sshipto.address_2,
	        "CITY":			sshipto.city,
	        "STATE":		sshipto.state,
	        "CNTRY":		sshipto.country,
	        "ZIP_COD":		sshipto.postcode,
	        "PHONE_1":		sshipto.phone,
		"NAM_TYP":		nams_typ
		},
	     "PS_DOC_LIN": [],
	     "PS_DOC_HDR_MISC_CHRG":  [
	        {
		"TOT_TYP":	"O",
		"MISC_CHRG_NO":	"1",
		"MISC_TYP":	"A",
		"MISC_AMT":	shippingtotal
		},
	        {
		"TOT_TYP":	"O",
		"MISC_CHRG_NO":	"4",
		"MISC_TYP":	"A",
		"MISC_AMT":	discount
		}],
	     "PS_DOC_PMT":  [
	     {
	        "AMT":	s.total,
	        "PAY_COD":	s.payment_method.toUpperCase(),
	        "FINAL_PMT":	"N"
	     }],
	     "PS_DOC_TAX": [
	     {
	      "AUTH_COD":	"AVALARA",
	      "RUL_COD":	"AVALARA",
	      "TAX_DOC_PART":	"O",
	      "TAX_AMT":	s.total_tax,
	      "TOT_TAXBL_AMT":	taxbl
	     }],
	     "PS_DOC_HDR_TOT": {
		"HAS_TAX_OVRD":	"A",
//	        "ORD_NORM_TAX_AMT":	s.total_tax,
//	        "ORD_TAX_AMT":	s.total_tax
	     },
	     "PS_TAX": {
//	        "ORD_NORM_TAX_AMT":	s.total_tax,
//	        "ORD_TAX_AMT":	s.total_tax
	     }
	    };
	if(s.shipping_tax === 0) {
	    PS_DOC_HDR.PS_DOC_TAX.TXBL_MISC_CHG_AMT_1=s.shipping_total;
	    }
	let co_retail_delivery_fee=0;
	for (const t of (s.tax_lines)){
	    if(t.rate_code === 'AVATAX-Retail-Delivery-Fee')co_retail_delivery_fee=1;
	    }
	let composite_items=[];
//	for (const i of s.line_items){
//	    if(i.composite_parent !== '')composite_items[i.sku]=1;
//	    }
	let paidsm=0;
	for (const i of s.line_items){
	    if(sales.sales[i.sku] === '1' && i.price !== 0) paidsm=1;
	    }
	let total_items=0;
	let sale_items=0;
        for (const i of s.line_items){
	    let lin_typ = "O";
	    let skipitem=0;
	    if(datab.bundles[i.sku]) bundleid[i.id]=i.sku;
 	    if(i.composite_parent !== '' && bundleid[i.composite_parent]
	    !=undefined){ // kit2?
 	    	const psku=bundleid[i.composite_parent];
 		for (const mm of datab.minmax[psku]){
 		    if(mm.sku === i.sku){
 		        if ((i.quantity === mm.min && i.quantity === mm.max )
 			   && i.price === 0){
 			       skipitem=1;
 			       }
 			}
 		    }
 	        }
	    if(skipitem === 0){
		total_items += 1;
	        const j=typeof(kits.kits1[i.sku]);
		let PRC_OVRD="Y";
	        if(printers.printers[i.sku] === '2'){
	            lin_typ = "S";
		    sale_items += 1;
		    }
	        if(printers.printers[i.sku] === '1'){
	            lin_typ = "D";
		    }
	        if(bundledrop.bundledrop[i.sku] === '1'){
	            lin_typ = "D";
		    }
	        else if ( j !== 'undefined'){
	            i.quantity = i.quantity * kits.kits1[i.sku].QTY_SOLD;
		    const q=kits.kits1[i.sku].QTY_SOLD;
		    i.sku = kits.kits1[i.sku].ITEM_NO;
		    PRC_OVRD="Y";
		    if(i.price !== 0) i.price = i.price / q;
		    }
//		if(sales.sales[i.sku] === '1') lin_typ= 'S';
	        PS_DOC_HDR.PS_DOC_LIN.push( {
	           "LIN_TYP":	lin_typ,
	           "ITEM_NO":	i.sku,
	           "PRC":		i.price,
	           "QTY_SOLD":	i.quantity,
	           "USR_ENTD_PRC":	"Y",
	           "HAS_PRC_OVRD":	PRC_OVRD,
	           "PFT_CTR":		"801-XXXXX"
	           });
	        if(bundles === 1 && datab.bundles[i.sku]){
		    bundleid[i.id]=i.sku;
	    	    for (let bb of datab.bundles[i.sku]){
		        if(kits.kits1[bb.ITEM_NO]){
		            bb.QTY_SOLD = bb.QTY_SOLD * kits.kits1[bb.ITEM_NO].QTY_SOLD;
			    bb.ITEM_NO = kits.kits1[bb.ITEM_NO].ITEM_NO;
			    }
			if(composite_items[bb.ITEM_NO] !== 1){
			    if(sales.sales[bb.ITEM_NO] === '1' && bb.PRC
			    === 0 && paidsm === 1){
			       }
			    else{
			        PS_DOC_HDR.PS_DOC_LIN.push(bb);
				}
			    }
		        }
		    }
	        const k=typeof(kits.kits2[i.sku]);
	        if(k !== 'undefined'){
	            for (const bb of kits.kits2[i.sku]){
		        PS_DOC_HDR.PS_DOC_LIN.push(bb);
		        }
		    }
	        }
	    }
	if(co_retail_delivery_fee === 1){
	        PS_DOC_HDR.PS_DOC_LIN.push( {
	           "LIN_TYP":	'O',
	           "ITEM_NO":	'VOPTC-O-801',
	           "PRC":		'0.00',
	           "QTY_SOLD":	'1',
	           "USR_ENTD_PRC":	'Y',
	           "HAS_PRC_OVRD":	'Y',
	           "PFT_CTR":		"801-XXXXX"
	           });
	    }
	if(sale_items > 0 && total_items !== sale_items){
	    getwoo.postnoteandstatus(s,`can't mix Special Sales items with regular orders`,'cp-stalled','1');
	    return(0);
	    }
	if(total_items === sale_items){
	    PS_DOC_HDR.DOC_TYP = "T";
	    PS_DOC_HDR.PS_DOC_HDR_TOT.SAL_NORM_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_DOC_HDR_TOT.SAL_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_TAX.SAL_NORM_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_TAX.SAL_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_DOC_HDR_MISC_CHRG[0].TOT_TYP="S";
	    PS_DOC_HDR.PS_DOC_HDR_MISC_CHRG[1].TOT_TYP="S";
	    PS_DOC_HDR.PS_DOC_TAX[0].TAX_DOC_PART="S";
	    PS_DOC_HDR.PS_DOC_PMT[0].FINAL_PMT="Y";
	    }
	else {
	    PS_DOC_HDR.PS_DOC_HDR_TOT.ORD_NORM_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_DOC_HDR_TOT.ORD_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_TAX.ORD_NORM_TAX_AMT=s.total_tax;
	    PS_DOC_HDR.PS_TAX.ORD_TAX_AMT=s.total_tax;
	    }
	if(verbose === 1){
	    console.log("PS_DOC_HDR.PS_DOC_LIN");
	    console.log(PS_DOC_HDR.PS_DOC_LIN);
	    }
	let PS_DOCS = {
	    PS_DOC_HDR,
	    };
    const results = {
        AR_CUST,
	AR_SHIP_ADRS,
	PS_DOCS
	};
    return(results);
    }
function checktktnos(ord_no,tkt_no,callback){
    console.log(`checktknos: ord_no=${ord_no}, tkt_no=${tkt_no}`);
    const sqlobjt0 = sqlobj.querysql(
    "SELECT NXT_ORD_NO,NXT_TKT_NO from PS_STR_CFG_PS WHERE STR_ID = '801'",
    (result) => {
        console.log(result.recordset[0]);
	const nxt_tkt_no=result.recordset[0].NXT_TKT_NO;
	const nxt_ord_no=result.recordset[0].NXT_ORD_NO;
	console.log(`nxt_tkt_no=${nxt_tkt_no}, tkt_no=${tkt_no}`);
	console.log(`nxt_ord_no=${nxt_ord_no}, ord_no=${ord_no}`);
	const sqlobjt1 = sqlobj.querysql(
	`select TKT_NO from PS_DOC_HDR where TKT_NO = '${ord_no}' or TKT_NO = '${tkt_no}'`,
	(r) => {
	    let tc=0;
	    let oc=0;
	    for (const t of r.recordset){
		if(t.TKT_NO === ord_no) oc++;
		else tc++;
	        }
	    console.log(`oc=${oc}, tc=${tc}`);
	    callback(ord_no,tkt_no);
	    });
	});
    }
let nxtorder='';
let nxttkt='';
let lastorder="00000";
let lasttkt="00000";
function checknexttktnos(callback){
//const sqlobjt2=sqlobj.querysql(
//"SELECT top 200 DOC_ID,TKT_NO from dbo.PS_DOC_HDR where STR_ID = '801' and TKT_NO like 'O801-%' OR TKT_NO like 'TKT801-%' ORDER BY TKT_DT DESC",
//(result) => {
//    for (const d of result.recordset){
//	tktprocess(d);
//	}
const sqlobjt3=sqlobj.querysql(
"SELECT top 200 DOC_ID,TKT_NO from dbo.PS_TKT_HIST where STR_ID = '801' and TKT_NO like 'O801-%' OR TKT_NO like 'TKT801-%' ORDER BY TKT_DT DESC",
(result) => {
    for (const d of result.recordset){
	tktprocess(d);
	}
const sqlobj1 = sqlobj.querysql(
    `SELECT NXT_ORD_NO,NXT_TKT_NO from PS_STR_CFG_PS WHERE STR_ID = '801'`,
    (result) => {
	let t=result.recordset[0].NXT_ORD_NO;
	let toa=t.split('-');
	nxtorder=toa[1];
	t=result.recordset[0].NXT_TKT_NO;
	let tta=t.split('-');
	nxttkt=tta[1];
	let status="bad";
	if(lastorder < nxtorder && lasttkt < nxttkt)status="good"
	if(verbose === 1) console.log( `${status},O801-${lastorder},=>,O801-${nxtorder},TKT801-${lasttkt},=>,TKT801-${nxttkt}`);
	    callback(status);
	    });
	    });
    }
function tktprocess(d){
    t=d.TKT_NO;
    let toa=t.split('-');
    if(toa[0] === 'TKT801'){
        if(toa[1] > lasttkt) lasttkt=toa[1];
        }
    }
