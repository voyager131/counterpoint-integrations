#!/bin/bash
curl --request POST \
  --url https://demo.spreecommerce.org/spree_oauth/token \
  --header 'Content-Type: application/json' \
  --data '{
  "grant_type": "password",
  "username": "spree@example.com",
  "password": "spree123"
}'
