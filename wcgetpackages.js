const getitems = require("./modules/wcitems.js");
const printers = require("./printers.js");
const sales = require("./sales.js");
const idtags = require("./packagetags.js");
let mylist = [];
let mylistlen = 0;
let done = 0;
let bundles = new Object();
let minmax = new Object();
function getmyitems(n,mycallback) {
	getitems.getwcitems(n,(r) => {
		let count=0;
		for ( const i of r.data){
			if(idtags.packagetags[i.sku] === '1'){
			    let bb = [];
			    let mm = [];
			    for (const cc of i.composite_components){
//				console.log(cc);
				const did=cc.default_option_id;
				const qty = cc.quantity_min;
				if(did !== '' && qty !== 0 && cc.optional !==
					true && cc.priced_individually === false ){
				     const sku=idtags.idlist[did];
				     let lin_typ='O';
				     if(printers.printers[sku] === '1')lin_typ='D';
//				     if(sales.sales[sku] === '1')lin_typ='S';
				     let myb = {
					 LIN_TYP: lin_typ,
				         ITEM_NO: sku,
//					 title: cc.title,
					 PRC: 0,
					 QTY_SOLD: qty,
					 USR_ENTD_PRC: 'Y',
					 HAS_PRC_OVRD: 'Y'
					 }
				     let mym = {
				         sku: sku,
					 min: cc.quantity_min,
					 max: cc.quantity_max
					 }
				     if(cc.quantity_max !== ''){
				         bb.push(myb);
				         mm.push(mym);
					 }
				     }
			        }
			    bundles[i.sku]=bb;
			    minmax[i.sku]=mm;
			    }
			mylist.push(i);
			mylistlen++;
			count++;
			}
		if(count === 50){
			n++;
			getmyitems(n,mycallback);
			}
		else {
			done = 1;
			}
		mycallback(r);
		});
	}
let page=1;
getmyitems(page,(r) => {
    if(done === 1){
    console.log("exports.bundles =");
    console.log(bundles);
    console.log("exports.minmax =");
    console.log(minmax);
        }
    });
