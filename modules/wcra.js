const vopdcwc = require('../configs/vopdcwc.js');
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
const mymailer = require('../configs/mymailer.js');
const api= new WooCommerceRestApi(vopdcwc.config);

exports.getwoo = (endp,n,mycallback) => {
api.get(endp,{
  per_page: n, // 20 orders per page
})
  .then((response) => {
    // Successful request
    mycallback(response);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log(error);
    mycallback(error);
  })
  .finally(() => {
    // Always executed.
  });
  };
exports.putstatus = (id,status,mycallback) => {
const myendpoint='orders/'+id;
api.put(myendpoint,{
  status: status
})
  .then((response) => {
    // Successful request
    mycallback(response);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log(error);
    mycallback(error);
  })
  .finally(() => {
    // Always executed.
  });
  }
function iputstatus(id,status,mycallback){
const myendpoint='orders/'+id;
api.put(myendpoint,{
  status: status
})
  .then((response) => {
    // Successful request
    mycallback(response);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log(error);
    mycallback(error);
  })
  .finally(() => {
    // Always executed.
  });
  }

exports.delwoo = (id,mycallback) => {
const myendpoint1='orders/'+id;
api.delete(myendpoint1,{
  force: true, 
})
  .then((response) => {
    // Successful request
    mycallback(response);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log(error);
    mycallback(error);
  })
  .finally(() => {
    // Always executed.
  });
  };

exports.deluwoo = (id,mycallback) => {
const myendpoint2='customers/'+id;
api.delete(myendpoint2,{
  force: true, 
})
  .then((response) => {
    // Successful request
    mycallback(response);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log(error);
    mycallback(error);
  })
  .finally(() => {
    // Always executed.
  });
  };

let urls = {
    'usps':	'https://tools.usps.com/go/TrackConfirmAction_input?strOrigTrackNum=',
    'ups':	'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=',
    'fedex':	'http://www.fedex.com/Tracking?tracknumbers='
    };
exports.posttrackingnote = (orderid,shipper,tracking,mycallback) => {
const myendpoint=`orders/${orderid}/notes`;
let myurl='';
console.log(`poasttrackingnot called with ${orderid}, ${shipper}, ${tracking}`);
if(tracking.includes('usps')){
    let l=tracking.length
    let s=l-22;
    let a=tracking.slice(0,s);
    let b=tracking.slice(s,l);
    myurl=urls.usps + ' ' + a + ' ' + b;
    }
if(tracking.includes('ups')){
    let l=tracking.length
    let s=l-18;
    let a=tracking.slice(0,s);
    let b=tracking.slice(s,l);
    myurl=urls.ups + ' ' + a + ' ' + b;
    }
if(tracking.includes('fedex')){
    let l=tracking.length
    let s=l-12;
    let a=tracking.slice(0,s);
    let b=tracking.slice(s,l);
    myurl=urls.fedex + ' ' + a + ' ' + b;
    }
const mesg=`Your Order #${orderid} has been shipped, ${shipper}, with a tracking number of ${tracking} or ${myurl}`;
console.log(`postgrackingnote message is ${mesg}`);
api.post(myendpoint, {
    note: mesg
    })
    .then((r) => { mycallback(r); })
    .catch((error) => {
        console.log(error);
        mycallback(error);
	})
    .finally(() => {});
    };
function postnotestatus(orderid,oldstatus,newstatus,mycallback){
const myendpoint=`orders/${orderid}/notes`;
const mesg=`Order status changed from ${oldstatus} to ${newstatus}`;
api.post(myendpoint, {
    note: mesg
    })
    .then((r) => { mycallback(r); })
    .catch((error) => {
        console.log(error);
        mycallback(r);
	})
    .finally(() => {});
    };
function postnotemesg(orderid,mesg,mycallback){
const myendpoint=`orders/${orderid}/notes`;
api.post(myendpoint, {
    note: mesg
    })
    .then((r) => { mycallback(r); })
    .catch((error) => {
        console.log(error);
        mycallback(r);
	})
    .finally(() => {});
    }
exports.postnoteandstatus = (s,mesg,newstatus,mailit) => {
	if(mailit === '1'){
	    mymailer.mymailer({
	    	to:	mymailer.voplist,
		from:	'(WooCommerce/CounterPoint Integration) <noreply@amcit.org>',
		subject:	'CounterPoint Integration error',
		text:	`While importing CounterPoint orders from DC store an Integration Error was found in order #${s.id} ${mesg}`
		});
	    }
	if(mesg !== ''){
	    postnotemesg(s.id,mesg, (r) => {
	        if(r.status !== 200 && r.status !== 201){
	            console.log(`post note failed for order ${s.id}`);
		    console.log(r);
		    }
	      });
	    }
	if(newstatus === '' || newstatus === s.status)return;
        postnotestatus(s.id,s.status,newstatus, (r) => {
	    if(r.status !== 200 && r.status !== 201){
	        console.log(`status change failed, ${s.status} => ${newstatus}`);
		console.log(r);
		}
	    });
	iputstatus(s.id,newstatus, (r) => {
		if(r.status === 200){
		    } else {
		        console.log(r);
		        }
	    });
	s.status = newstatus;
	return;
	}
