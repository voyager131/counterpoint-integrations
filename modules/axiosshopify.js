const https = require('https');
const axios = require('axios').default;
const shopify = require('../configs/shopify.js');

async function get(url,mycallback){
    const instance = axios.create(shopify.config);
    try {
      const response = await instance.get(`${url}`);
      response.status="";
      mycallback(response);
    } catch (error) {
      console.log("error");
      console.log(error);
//      error.status=error.response.status;
      error.status= -1;
      mycallback(error);
    }
  }

exports.myget = (url,cuno,mycallback) => {
  get(url,cuno,mycallback);
};

async function post(url,val,mycallback){
    const instance = axios.create(shopify.config);
    try {
      const response = await instance.post(`${url}`,val);
      response.status="";
      mycallback(response);
    } catch (error) {
//      console.log(`error in post ${url}`);
//      console.log(error);
      if(error.response === undefined){
      	error.status="unknown";
      } else {
        error.status=error.response.status;
      }
      mycallback(error);
    }
  }

exports.mypost = (url,cuno,mycallback) => {
  post(url,cuno,mycallback);
};

async function patch(url,val,mycallback){
    const instance = axios.create(shopify.config);
    try {
      const response = await instance.patch(`${url}`,val);
      mycallback(response);
    } catch (error) {
      console.log(error);
      console.log('end of error');
    }
  }

exports.mypatch = (url,cuno,mycallback) => {
  patch(url,cuno,mycallback);
};
