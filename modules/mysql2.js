const mysql = require('mysql2');
const mysqlc = require('../configs/mysql.js');

exports.doquery = (myquery,callback) => {
    const connection = mysql.createConnection(mysqlc.config);
    connection.promise().query(myquery)
        .then( ([rows,fields]) => {
	    callback(rows);
	    })
	.catch((r) => {
	    console.log(r);
	    callback(r);
	    })
	.finally( () => connection.end());
	}
exports.done = () => {
	connection.end();
	}
