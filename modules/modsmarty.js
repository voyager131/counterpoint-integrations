const smarty = require('../configs/smarty.js');

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()
    }
exports.addresscheck = (addr,callback) => {
let lookup = new smarty.Lookup();
lookup.street = addr.address_1;
lookup.street2 = addr.address_2;
lookup.city = addr.city;
lookup.state = addr.state
//console.log(`${addr.address_1}, ${addr.city} ${addr.state} ${addr.postcode}`);
lookup.maxCandidates = 10;

smarty.client.send(lookup)
	.then(handleSuccess)
	.catch(handleError); 
function handleSuccess(response) {
	let lookup = response.lookups[0];
	let C=lookup.result[0];
	let address_1=C.deliveryLine1;
	let address_2=C.deliveryLine2;
	let lastline=C.lastLine;
	let a1array=addr.address_1.split(" ");
	let a2array=address_1.split(" ");
	let i=0;
	let match=1;
	while(i<a1array.length){
		if(a1array[i] !== a2array[i]){
			if(a1array[i] === 'Lane' && a2array[i] === 'Ln'){
				a1array[i]='Ln';
				}
			else if((a1array[i].toLowerCase() === 'street' || a1array[i].toLowerCase() === 'st')
			    && a2array[i] === 'St'){
				a1array[i]='St';
				}
			else if(a1array[i] === 'St.' && a2array[i] === 'St'){
				a1array[i]='St';
				}
			else if(a1array[i] === 'Circle' && a2array[i] === 'Cir'){
				a1array[i]='Cir';
				}
			else if(a1array[i] === 'East' && a2array[i] === 'E'){
				a1array[i]='E';
				}
			else if(a1array[i] === 'Blvd.' && a2array[i] === 'Blvd'){
				a1array[i]='Blvd';
				}
			else if((a1array[i] === 'Rd.' || a1array[i] === 'RD' || a1array[i] === 'Road')
			    && a2array[i] === 'Rd'){
				a1array[i]='Rd';
				}
			else if(a1array[i] === 'E.' && a2array[i] === 'E'){
				a1array[i]='E';
				}
			else if(a1array[i] === 'S.' && a2array[i] === 'S'){
				a1array[i]='S';
				}
			else if(a1array[i] === 'N.' && a2array[i] === 'N'){
				a1array[i]='N';
				}
			else if(a1array[i] === 'W.' && a2array[i] === 'W'){
				a1array[i]='W';
				}
			else if(a1array[i] === 'Drive' && a2array[i] === 'Dr'){
				a1array[i]='Dr';
				}
			else if(a1array[i] === 'Ave.' && a2array[i] === 'Ave'){
				a1array[i]='Ave';
				}
			else if(a1array[i] === 'Square' && a2array[i] === 'Sq'){
				a1array[i]='Sq';
				}
			else if(a1array[i] === 'Avenue' && a2array[i] === 'Ave'){
				a1array[i]='Ave';
				}
			else if(a1array[i] === 'Terrace' && a2array[i] === 'Ter'){
				a1array[i]='Ter';
				}
			else if(a1array[i] === 'sw' && a2array[i] === 'SW'){
				a1array[i]='SW';
				}
			else if(a1array[i] === 'nw' && a2array[i] === 'NW'){
				a1array[i]='NW';
				}
			else if(a1array[i] === 'ne' && a2array[i] === 'NE'){
				a1array[i]='NE';
				}
			else if(a1array[i] === 'se' && a2array[i] === 'SE'){
				a1array[i]='SE';
				}
			else if(a1array[i] === 'po' && a2array[i] === 'PO'){
				a1array[i]='PO';
				}
			else if(a1array[i].toLowerCase() === 'box' && a2array[i] === 'Box'){
				a1array[i]='Box';
				}
			else if(a1array[i] === 'north' && a2array[i] === 'N'){
				a1array[i]='N';
				}
			else if(a1array[i] === 'south' && a2array[i] === 'S'){
				a1array[i]='S';
				}
			else if(a1array[i] === 'east' && a2array[i] === 'E'){
				a1array[i]='E';
				}
			else if(a1array[i] === 'west' && a2array[i] === 'W'){
				a1array[i]='W';
				}
			else if(a1array[i] === 'St.' && a2array[i] === 'Saint'){
				a1array[i]='Saint';
				}
			else if(capitalize(a1array[i]) === a2array[i]){
				a1array[i]=capitalize(a1array[i]);
				}
			else match=0;
			}
		i+=1;
		}
	addr.address_1=a1array.join(' ');
	let line=addr.city + ' ' + addr.state + ' ' + addr.postcode;
	a1array=line.split(" ");
	a2array=C.lastLine.split(' ');
	i=0;
	while(i<a1array.length){
		if(a1array[i] !== a2array[i]){
			if(a2array[i].includes('-')){
				let zips=a2array[i].split('-');
				if(zips[0] === addr.postcode){
					addr.postcode=a2array[i];
					}
				else {
					match=0;
					}
				}
			else match=0;
			}
		i+=1;
		}
//	console.log(`    ${addr.address_1}, ${addr.city} ${addr.state} ${addr.zipcode}`);
//	lookup.result.map(candidate => console.log(`    ${candidate.deliveryLine1}, ${candidate.lastLine}`));
//	if(match === 1) console.log ("match is good");
//	else console.log("match fails");
	callback(addr,C,match);
	}
function handleError(response) {
	console.log(response);
	}
}
