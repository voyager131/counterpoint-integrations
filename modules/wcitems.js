const vopdcwc = require('../configs/vopdcwc.js');

exports.getwcitems = (n,mycallback) => {
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;

const api = new WooCommerceRestApi(vopdcwc.config);
api.get('products?', {
  per_page: 50,
  page: n})
  .then((response) => {
    // Successful request
    mycallback(response);
  })
  .catch((error) => {
    // Invalid request, for 4xx and 5xx statuses
    console.log(error);
  })
  .finally(() => {
    // Always executed.
  });
  };
