const getitems = require("./modules/wcitems.js");
let mylist = [];
let mylistlen = 0;
let done = 0;
console.log("exports.idlist = {");
function getmyitems(n,mycallback) {
	getitems.getwcitems(n,(r) => {
		let count=0;
		for ( const i of r.data){
			if(i.sku !== '')console.log(`    '${i.id}': '${i.sku}',`);
			mylist.push(i);
			mylistlen++;
			count++;
			}
		if(count === 50){
			n++;
			getmyitems(n,mycallback);
			}
		else {
			done = 1;
			}
		mycallback(r);
		});
	}

let page=1;
let packagetags = new Object();
getmyitems(page,(r) => {
    if(done === 1){
    for (const i of mylist){
        if(i.sku === ""){
            continue;
            }
        for (const j of i.tags){
	    if(j.name.includes('Package')){
	        if(!j.name.includes('Booster'))packagetags[i.sku]='1';
		}
            }
        }
    console.log(`    '0' : 'none' }`);
    console.log("exports.packagetags =");
    console.log(packagetags);
        }
    });
