const sqlobj = require("./modules/asyncminimal.js");
let fixit=0;
let verbose=0;
let staticorder=0;
let staticticket=0;
for (const a of process.argv.slice(2)){
    if(a === "fixit"){
        fixit = 1;
	}
    else if (a === "verbose"){
        verbose=1;
	}
    else {
        let st=a.split(':');
	staticorder=st[0];
	staticticket=st[1];
	}
    }
let nxtorder='';
let nxttkt='';
let lastorder="00000";
let lasttkt="00000";
let tta=[2];
console.log(`staticorder=${staticorder}, staticticket=${staticticket}`);
const sqlobj1 = sqlobj.querysql( `SELECT NXT_ORD_NO,NXT_TKT_NO from PS_STR_CFG_PS WHERE STR_ID = '801'`,
    (result) => {
	let t=result.recordset[0].NXT_ORD_NO;
	let toa=t.split('-');
	nxtorder=toa[1];
	t=result.recordset[0].NXT_TKT_NO;
	tta=t.split('-');
	nxttkt=tta[1];
const sqlobjt2=sqlobj.querysql( `SELECT top 1 (TKT_NO) from ( select TKT_NO from dbo.PS_DOC_HDR where STR_ID = '801' and TKT_NO like 'O801-%' Union Select TKT_NO From PS_TKT_HIST where STR_ID = '801' and TKT_NO like '%O801%') results ORDER BY substring(TKT_NO, 6,5) desc`,
(result) => {
    for (const d of result.recordset){
	if(verbose === 1)console.log(`doc ${d}`);
	tktprocess(d);
	}
const sqlobjt3=sqlobj.querysql( `SELECT top 1 (TKT_NO) from ( select TKT_NO from dbo.PS_DOC_HDR where STR_ID = '801' and TKT_NO like 'T801-%' Union Select TKT_NO From PS_TKT_HIST where STR_ID = '801' and TKT_NO like '%T801%' and TKT_DT > '9/1/2022') results ORDER BY substring(TKT_NO, 6,5) desc`,
(result) => {
    for (const d of result.recordset){
	if(verbose === 1)console.log(`hist: ${d}`);
	tktprocess(d);
	}
	console.log(`O801-${nxtorder}, ${tta[0]}-${nxttkt}`);
	    console.log(`last order is O801-${lastorder}, NXT_ORD_NO=O801-${nxtorder}\nlast ticket is ${tta[0]}-${lasttkt}, NXT_TKT_NO=${tta[0]}-${nxttkt}`);
	    if(lastorder < nxtorder && lasttkt < nxttkt)console.log("Orders and Tickets are good!");
	    else console.log("Woah, we have a problem here!");
	if(fixit === 1){
	    console.log(`lastorder=${lastorder}, nxtorder=${nxtorder}`);
	    if(lastorder >= nxtorder){
	        const nxo=parseInt(lastorder)+1;
	        const nxot='O801-'+nxo;
	        console.log(`UPDATE PS_STR_CFG_PS set NXT_ORD_NO = '${nxot}' where STR_ID = '801'`);
	        sqlobj2=sqlobj.querysql(`UPDATE PS_STR_CFG_PS set NXT_ORD_NO = '${nxot}' where STR_ID = '801'`,
		(r) => {
		console.log(r)});
		}
	    if(lasttkt >= nxttkt){
	        const nxt=parseInt(lasttkt)+1;
		const nxtt=tta[0]+'-'+nxt;
	        sqlobj3=sqlobj.querysql(`UPDATE PS_STR_CFG_PS set NXT_TKT_NO = '${nxtt}' where STR_ID = '801'`,
		(r) => {});
		}
	    }
	    });
	    });
	});
function tktprocess(d){
    t=d.TKT_NO;
    let stoa=t.split('-');
    if(stoa[0] === 'O801'){
        if(stoa[1] > lastorder)lastorder=stoa[1];
        }
    else if(stoa[0] === tta[0]){
        if(stoa[1] > lasttkt) lasttkt=stoa[1];
        }
    }
